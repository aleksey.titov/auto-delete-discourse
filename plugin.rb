# name: discourse-auto-deletion
# about: Automatically deletions inactive users so that they need to recomfirm their email in order to login in again
# version: 0.0.1
# authors: aleksey titov
# url: https://gitlab.com/aleksey.titov/auto-delete-discou

enabled_site_setting :auto_deletion_enabled

PLUGIN_NAME ||= 'discourse_auto_deletion'.freeze

after_initialize do
  module ::DiscourseAutodeletion
    class Engine < ::Rails::Engine
      engine_name PLUGIN_NAME
      isolate_namespace DiscourseAutodeletion
    end
  end

  module ::Jobs
    class AutodeletionUsers < Jobs::Scheduled
      every 1.day

      def self.to_deletion
        auto_deletion_days = SiteSetting.auto_deletion_after_days.days.ago
        to_deletion = User.where("(last_seen_at IS NULL OR last_seen_at < ?) AND created_at < ?", auto_deletion_days, auto_deletion_days)
                            .where('active = ?', true)
                            .real
      end

      def self.exclude_users_in_safe_groups(deletion_list)
        safe_groups = SiteSetting.auto_deletion_safe_groups
        safe_to_deletion = []
        for user in deletion_list do
          safe_to_deletion << user if !(user.groups.any? { |g| safe_groups.include? g.name })
        end
        safe_to_deletion
      end

      def execute(args)
        return if !SiteSetting.auto_deletion_enabled?

        deletion_list = self.class.to_deletion
        safe_to_deletion = self.class.exclude_users_in_safe_groups(deletion_list)
        for user in safe_to_deletion do
          user.active = false
          deletion_reason = I18n.t("discourse_auto_deletion.deletion-reason")

          if user.save
            StaffActionLogger.new(Discourse.system_user).log_user_deletion(user)
          end
        end
      end
    end
  end
end
