# discourse-auto-deletion
A plugin for Discourse which automatically deletion users after a defined period of inactivity.

[How to install a plugin](https://meta.discourse.org/t/install-a-plugin/19157)

The plugin is disabled by default, make sure to enable it in your site settings.